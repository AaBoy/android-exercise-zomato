Android App Coding Challenge
Coding Brief
An Android app that uses the zomato API to display restaurants in Abbotsford
Deliverable
● A link to a github project containing your work.
● A project/workspace that will build and run in Android Studio 3.1.
● Must use git. Don’t be afraid to push WIP commits.

Part 1 - Restaurant List
Use the zomato api (​https://developers.zomato.com/api​) to display a list of the​ 10 best rated restaurants in Abbotsford, Victoria.
Provide the name of the restaurant and the address as shown from the API.
Design Notes
The restaurant cell consists of:
● the restaurant’s Feature Image as a background image
● a gradient layer
● The restaurant name and
address.
It doesn’t need to look exactly like in the image but as close as you can get.
Title/Restaurant Name
System font bold, Size 16, white, left aligned
Vertical Gradient
Alpha: 0% top - 70% bottom Colour: #000
Address
System font regular, Size 10, white, left aligned

Part 2 - Persistence
Use image caching to ensure the images only download once.
Use your choice of database framework to show the latest data fetched from the API if the app does not have access to the internet.

Part 3 (Optional) Implement a favourites system
Using tab navigation, add a favourites tab that displays favourite restaurants. Use the same design as the first tab, but add a favourite/unfavourite button to the restaurant cells on both screens.
Tapping favourite/unfavourite should toggle the button. Upon tapping, the favourites screen should be updated to add/remove restaurants so that it only displays favourite restaurants.
Favourite Restaurants should also be persisted.