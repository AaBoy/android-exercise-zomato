package pp.androidzomato.utils

import pp.androidzomato.data.Location
import pp.androidzomato.data.Restaurant
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.data.SearchRestaurants

object TestUtilUI {

    fun createSearchResult(id: String = "id", name: String = "name") = SearchRestaurants(
            restaurants = arrayListOf(Restaurant(RestaurantX(id, name,
                    "featured_image",
                    false,
                    Location("address"))))
    )

    fun createFavoriteList(id: String = "id", name: String = "name") =
            arrayListOf(RestaurantX(id, name,
                    "featured_image",
                    true,
                    Location("address"))
            )

    fun createList(id: String = "id", name: String = "name") =
            arrayListOf(RestaurantX(id, name,
                    "featured_image",
                    false,
                    Location("address"))
            )
}