package pp.androidzomato.ui

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.AdditionalMatchers.not
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import pp.androidzomato.R
import pp.androidzomato.SingleFragmentActivity
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.helper.Resource
import pp.androidzomato.ui.fragment.RestaurantsFragment
import pp.androidzomato.ui.viewmodel.RestaurantViewModel
import pp.androidzomato.utils.*

@RunWith(AndroidJUnit4::class)
class RestaurantsFragmentTest {
    @get:Rule
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java)

    @get:Rule
    val executeRule = TaskExecutorWithIdlingResourceRule()

    @get:Rule
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    private val restaurantLiveData = MutableLiveData<Resource<List<RestaurantX>>>()
    private lateinit var viewModel: RestaurantViewModel
    private val fragment = RestaurantsFragment.newInstance()

    @Before
    fun init() {
        viewModel = mock(RestaurantViewModel::class.java)

        `when`(viewModel.getRestaurants()).thenReturn(restaurantLiveData)
        fragment.viewModelFactory = ViewModelUtil.createFor(viewModel)

        activityRule.activity.setFragment(fragment)
    }

    @Test
    fun mock() {
        onView(withId(R.id.recycler_view)).check(matches(not(isDisplayed())))
    }

    @Test
    fun showList() {
        restaurantLiveData.postValue(Resource.success(TestUtilUI.createList()))
        onView(withId(R.id.list_item)).check(matches(isDisplayed()))
    }

    @Test
    fun updateItem() {
        val items = TestUtilUI.createList().toMutableList()
        restaurantLiveData.postValue(Resource.success(items))

        onView(withId(R.id.list_item)).check(matches(isDisplayed()))

        val newItem = TestUtilUI.createList().toMutableList()
        newItem[0].isFavorite = true
        restaurantLiveData.postValue(Resource.success(newItem))
        onView(listMatcher().atPosition(0))
                .check(matches(hasDescendant(withText(R.string.unfavored))))
        onView(withId(R.id.favorite)).check(matches(withText(R.string.unfavored)))
    }

    private fun listMatcher():RecyclerViewMatcher{
        return RecyclerViewMatcher(R.id.recycler_view)
    }
}