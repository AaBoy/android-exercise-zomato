package pp.androidzomato.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.utils.TestUtilUI
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class RestaurantDaoTest {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    lateinit var database: RestaurantDataBase

    lateinit var restaurant: RestaurantX

    @Mock
    lateinit var restunrats: LiveData<List<RestaurantX>>

    @Before
    fun beforeTest() {
        restaurant = TestUtilUI.createSearchResult().restaurants.get(0).restaurant
        database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                RestaurantDataBase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @Test
    fun insert() {
        database.getRestaurantDao().insertRestaurants(listOf(restaurant))
        val result = database.getRestaurantDao().getRestaurants().blockingObserve()
        assert(restaurant.name == result?.get(0)?.name)
    }

    @Test
    fun update() {
        database.getRestaurantDao().insertRestaurants(listOf(restaurant))
        restaurant.isFavorite = true
        database.getRestaurantDao().updateRestaurant(restaurant.isFavorite, restaurant.id)
        val somevalue = database.getRestaurantDao().getFavoriteRestaurants().blockingObserve()
        MatcherAssert.assertThat(somevalue?.get(0)?.isFavorite, CoreMatchers.`is`(true))
    }

    @Test
    fun updateWithLiveData() {
        database.getRestaurantDao().insertRestaurants(listOf(restaurant))
        val somevalue = database.getRestaurantDao().getRestaurants()

        restaurant.isFavorite = true
        database.getRestaurantDao().updateRestaurant(restaurant.isFavorite, restaurant.id)

        MatcherAssert.assertThat(somevalue.blockingObserve()?.size, CoreMatchers.`is`(1))
        MatcherAssert.assertThat(somevalue.blockingObserve()?.get(0)?.isFavorite, CoreMatchers.`is`(true))
    }
}

fun <T> LiveData<T>.blockingObserve(): T? {
    var value: T? = null
    val latch = CountDownLatch(1)
    val innerObserver = Observer<T> {
        value = it
        latch.countDown()
    }
    observeForever(innerObserver)
    latch.await(2, TimeUnit.SECONDS)
    return value
}