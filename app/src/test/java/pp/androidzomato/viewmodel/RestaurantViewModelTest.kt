package pp.androidzomato.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.google.common.truth.Truth
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.helper.Resource
import pp.androidzomato.ui.repo.RestaurantRepository
import pp.androidzomato.ui.viewmodel.RestaurantViewModel
import pp.androidzomato.utils.testObserverValue

@RunWith(JUnit4::class)
class RestaurantViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val repository = Mockito.mock(RestaurantRepository::class.java)
    val restaurantViewModel = RestaurantViewModel(repository)
    val repoLiveData = MutableLiveData<Resource<List<RestaurantX>>>()

    @Before
    fun setup() {
        Mockito.`when`(repository.getFavorite()).thenReturn(repoLiveData)
    }

    @Test
    fun getAllRestaurants() {
        repoLiveData.postValue(Resource.loading(null))
        val data = restaurantViewModel.getFavorite().testObserverValue()
        Truth.assert_()
                .that(data.observedValues[0]?.data?.get(0)?.id)
                .isEqualTo(repoLiveData.value?.data?.get(0)?.id)

        Mockito.verify(repository, Mockito.times(1)).getFavorite()
    }
}