package pp.androidzomato.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import pp.androidzomato.api.ZomatoApi
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.data.SearchRestaurants
import pp.androidzomato.db.RestaurantDao
import pp.androidzomato.helper.Resource
import pp.androidzomato.ui.repo.RestaurantRepository
import pp.androidzomato.utils.InstantAppExecutors
import pp.androidzomato.utils.TestUtil
import pp.androidzomato.utils.mock
import retrofit2.Call
import retrofit2.Response

@RunWith(JUnit4::class)
class RestaurantRepositoryTest {

    private val dao = Mockito.mock(RestaurantDao::class.java)
    private val api = Mockito.mock(ZomatoApi::class.java)
    private val repo = RestaurantRepository(api, dao, InstantAppExecutors())

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun getRestaurantsList() {
        val dbData = MutableLiveData<List<RestaurantX>>()
        Mockito.`when`(dao.getRestaurants()).thenReturn(dbData)
        val searchRestaurants = TestUtil.createSearchResult()

        val call = mock<Call<SearchRestaurants>>()
        Mockito.`when`(call.execute()).thenReturn(Response.success(searchRestaurants))
        Mockito.`when`(api.getRestaurants()).thenReturn(call)
        val observer = mock<Observer<Resource<List<RestaurantX>>>>()

        repo.getRestaurants().observeForever(observer)
        Mockito.verify(api, Mockito.never()).getRestaurants()
        val updatedDbData = MutableLiveData<List<RestaurantX>>()
        Mockito.`when`(dao.getRestaurants()).thenReturn(updatedDbData)
        dbData.value = null
        Mockito.verify(api).getRestaurants()
    }

    @Test
    fun getRestaurantsFavoriteList() {
        val dbData = MutableLiveData<List<RestaurantX>>()
        dbData.value = TestUtil.createFavoriteList()
        Mockito.`when`(dao.getFavoriteRestaurants()).thenReturn(dbData)

        val observer = mock<Observer<Resource<List<RestaurantX>>>>()

        repo.getFavorite().observeForever(observer)
        Mockito.verify(dao, Mockito.times(1)).getFavoriteRestaurants()
    }
}
