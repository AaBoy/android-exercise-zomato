package pp.androidzomato.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import pp.androidzomato.data.Location
import java.io.Serializable
import javax.inject.Singleton

@Singleton
class DataConvertRestaurant : Serializable {

    @TypeConverter
    fun fromLocation(location: Location?): String? {
        if (location == null) {
            return null
        }
        return Gson().toJson(location, Location::class.java)
    }

    @TypeConverter // note this annotation
    fun toOptionValueLocation(optionValuesString: String?): Location? {
        if (optionValuesString == null) {
            return null
        }
        return Gson().fromJson<Location>(optionValuesString, Location::class.java)
    }
}