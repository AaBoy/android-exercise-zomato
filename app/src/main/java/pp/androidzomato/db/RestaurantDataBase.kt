package pp.androidzomato.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pp.androidzomato.data.RestaurantX
import javax.inject.Singleton

@Singleton
@Database(entities = arrayOf(RestaurantX::class), version = 1, exportSchema = false)
@TypeConverters(DataConvertRestaurant::class)
abstract class RestaurantDataBase : RoomDatabase() {

    abstract fun getRestaurantDao():RestaurantDao
}