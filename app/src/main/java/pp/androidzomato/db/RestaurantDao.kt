package pp.androidzomato.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pp.androidzomato.data.Restaurant
import pp.androidzomato.data.RestaurantX

@Dao
interface RestaurantDao {

    @Query("SELECT * FROM restaurant")
    fun getRestaurants(): LiveData<List<RestaurantX>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertRestaurants(restaurantX: List<RestaurantX>)

    @Query("UPDATE restaurant SET is_favorite=:isFavorite WHERE id=:id")
    fun updateRestaurant(isFavorite: Boolean, id: String)

    @Query("SELECT * FROM restaurant WHERE is_favorite=1")
    fun getFavoriteRestaurants(): LiveData<List<RestaurantX>>

}