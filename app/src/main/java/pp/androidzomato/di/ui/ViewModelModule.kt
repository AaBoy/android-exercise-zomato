package pp.androidzomato.di.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pp.androidzomato.di.viewModelHelper.ViewModelFactory
import pp.androidzomato.di.viewModelHelper.ViewModelKey
import pp.androidzomato.ui.viewmodel.RestaurantViewModel

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RestaurantViewModel::class)
    internal abstract fun bindRestaurantViewModel(restaurantViewModel: RestaurantViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}