package pp.androidzomato.di.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pp.androidzomato.ui.fragment.FavoriteRestaurantFragment
import pp.androidzomato.ui.fragment.RestaurantsFragment

@Module
abstract class RestaurantActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeRestaurantsFragment(): RestaurantsFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteRestaurantFragment(): FavoriteRestaurantFragment
}
