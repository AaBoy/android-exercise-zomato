package pp.androidzomato.di.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pp.androidzomato.ui.RestaurantActivity

@Module
abstract class DaggerActivityBinding {

    @ContributesAndroidInjector(modules = [RestaurantActivityModule::class])
    abstract fun bindRestaurantActivity(): RestaurantActivity
}