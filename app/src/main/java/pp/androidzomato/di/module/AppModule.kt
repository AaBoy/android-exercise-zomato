package pp.androidzomato.di.module

import android.app.Application
import androidx.room.Room
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import pp.androidzomato.api.ZomatoApi
import pp.androidzomato.db.RestaurantDao
import pp.androidzomato.db.RestaurantDataBase
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun providesContest(app: Application): Context = app

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val build = OkHttpClient.Builder()
        build.addInterceptor(Interceptor { chain ->
            val request: Request = chain.request()

            val newRequest: Request.Builder = request.newBuilder().header("user-key", "63d8729df566d19d3b0a8b4465824214")

            return@Interceptor chain.proceed(newRequest.build())
        })

        return build.build()
    }


    @Provides
    @Singleton
    fun provideApiService(gson: Gson, okHttpClient: OkHttpClient): ZomatoApi =
            Retrofit.Builder()
                    .baseUrl("https://developers.zomato.com/api/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build().create(ZomatoApi::class.java)


    @Provides
    @Singleton
    fun provideRestaurantDatabase(app: Application): RestaurantDataBase =
            Room.databaseBuilder(app.applicationContext,
                    RestaurantDataBase::class.java,
                    "gostilna.restaurantDao").build()

    @Provides
    @Singleton
    fun provideRestaurantDao(database: RestaurantDataBase): RestaurantDao = database.getRestaurantDao()
}