package pp.androidzomato.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pp.androidzomato.app.ZomatoApp
import pp.androidzomato.di.module.AppModule
import pp.androidzomato.di.ui.DaggerActivityBinding
import pp.androidzomato.di.ui.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class, ViewModelModule::class, DaggerActivityBinding::class))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: ZomatoApp)
}