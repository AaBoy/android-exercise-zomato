package pp.androidzomato.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "restaurant")
data class RestaurantX(
        @PrimaryKey
        val id: String,
        var name: String,
        val featured_image: String,
        @ColumnInfo(name="is_favorite")
        var isFavorite:Boolean,
        val location: Location
)