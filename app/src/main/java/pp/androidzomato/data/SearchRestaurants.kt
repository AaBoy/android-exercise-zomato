package pp.androidzomato.data

data class SearchRestaurants(
        val restaurants: List<Restaurant>
)