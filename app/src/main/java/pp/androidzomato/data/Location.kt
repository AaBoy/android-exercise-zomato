package pp.androidzomato.data

data class Location(
        val address: String
)