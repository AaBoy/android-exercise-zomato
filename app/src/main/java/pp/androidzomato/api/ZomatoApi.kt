package pp.androidzomato.api

import pp.androidzomato.data.SearchRestaurants
import retrofit2.Call
import retrofit2.http.GET

interface ZomatoApi{
    @GET("v2.1/search?start=0&count=10&sort=rating&order=desc")
    fun getRestaurants(): Call<SearchRestaurants>
}