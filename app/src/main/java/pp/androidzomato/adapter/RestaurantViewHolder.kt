package pp.androidzomato.adapter

import androidx.recyclerview.widget.RecyclerView
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.databinding.ItemRestaurantBinding
import pp.androidzomato.helper.IClickEvent

class RestaurantViewHolder(val binding: ItemRestaurantBinding, val iClickEvent: IClickEvent) : RecyclerView.ViewHolder(binding.root) {

    fun bind(restaurant: RestaurantX) {
        binding.restaurant = restaurant
        binding.click=iClickEvent
        binding.notifyChange()
    }
}