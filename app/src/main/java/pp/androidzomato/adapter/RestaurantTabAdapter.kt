package pp.androidzomato.adapter

import android.app.Application
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import pp.androidzomato.R
import pp.androidzomato.ui.fragment.FavoriteRestaurantFragment
import pp.androidzomato.ui.fragment.RestaurantsFragment

class RestaurantTabAdapter(val fm: FragmentManager, val application: Application) : FragmentStatePagerAdapter(fm) {

    lateinit var restaurant: RestaurantsFragment
    lateinit var favoriteRestaurant: FavoriteRestaurantFragment

    init {
        restaurant = RestaurantsFragment.newInstance()
        favoriteRestaurant = FavoriteRestaurantFragment.newInstance()
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return restaurant
            1 -> return favoriteRestaurant
        }

        return RestaurantsFragment.newInstance()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return application.getString(R.string.all_restaurants)
            1 -> return application.getString(R.string.favorite_retaurants)
        }

        return application.getString(R.string.error_tab_text)
    }

    override fun getCount(): Int {
        return 2
    }

}