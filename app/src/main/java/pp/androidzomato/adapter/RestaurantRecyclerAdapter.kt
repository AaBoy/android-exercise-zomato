package pp.androidzomato.adapter


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pp.androidzomato.R
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.databinding.ItemRestaurantBinding
import pp.androidzomato.helper.IClickEvent

const val UPDATE_BUTTON = "UPDATE BUTTON"

class RestaurantRecyclerAdapter(val iClickEvent: IClickEvent) : RecyclerView.Adapter<RestaurantViewHolder>() {
    var list: List<RestaurantX> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val binding = DataBindingUtil.inflate<ItemRestaurantBinding>(LayoutInflater.from(parent.context), R.layout.item_restaurant, parent, false)
        return RestaurantViewHolder(binding, iClickEvent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
            return
        } else {
            val bundle = payloads.get(0) as Bundle
            for (key: String in bundle.keySet()) {
                when (key) {
                    UPDATE_BUTTON -> {holder.bind(list[position])}
                }
            }
        }
    }

    fun setNewData(tempList: List<RestaurantX>) {
        val diffResult = DiffUtil.calculateDiff(MyDiffCallback(list, tempList))
        diffResult.dispatchUpdatesTo(this)
        list = tempList
    }
}

class MyDiffCallback(val odlRestaurants: List<RestaurantX>, val newRestaurants: List<RestaurantX>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return odlRestaurants.size
    }

    override fun getNewListSize(): Int {
        return newRestaurants.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newRestaurants[newItemPosition].equals(odlRestaurants[oldItemPosition])
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return odlRestaurants[oldItemPosition].id.equals(newRestaurants[newItemPosition].id)
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val diffBundle = Bundle()
        diffBundle.putBoolean(UPDATE_BUTTON, newRestaurants[newItemPosition].isFavorite)
        return diffBundle
    }
}