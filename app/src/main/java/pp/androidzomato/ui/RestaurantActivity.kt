package pp.androidzomato.ui

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import pp.androidzomato.R
import pp.androidzomato.adapter.RestaurantTabAdapter
import pp.androidzomato.databinding.ActivityRestaurantBinding
import pp.androidzomato.helper.BaseActivity

class RestaurantActivity : BaseActivity() {
    lateinit var binding: ActivityRestaurantBinding
    lateinit var restaurantTabAdapter: RestaurantTabAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_restaurant)

        restaurantTabAdapter = RestaurantTabAdapter(supportFragmentManager, application)
        binding.viewPager.adapter = restaurantTabAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }
}
