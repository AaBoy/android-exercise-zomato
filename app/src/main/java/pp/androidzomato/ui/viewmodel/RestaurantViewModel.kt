package pp.androidzomato.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import pp.androidzomato.OpenForTesting
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.helper.Resource
import pp.androidzomato.ui.repo.RestaurantRepository
import javax.inject.Inject

@OpenForTesting
class RestaurantViewModel @Inject constructor(val repository: RestaurantRepository) : ViewModel() {
    fun getRestaurants(): LiveData<Resource<List<RestaurantX>>> {
        return repository.getRestaurants()
    }

    fun getFavorite(): LiveData<Resource<List<RestaurantX>>> {
        return repository.getFavorite()
    }

    fun updateRestaurantFavoriteStatus(restaurantX: RestaurantX) {
        repository.setFavoriteStatus(restaurantX)
    }
}