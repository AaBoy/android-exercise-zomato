package pp.androidzomato.ui.fragment

import androidx.lifecycle.Observer
import pp.androidzomato.OpenForTesting

@OpenForTesting
class FavoriteRestaurantFragment : RestaurantsFragment() {
    companion object {
        fun newInstance() = FavoriteRestaurantFragment()
        val TAG: String = FavoriteRestaurantFragment.toString()
    }

    override fun setUpViewObserver() {
        viewModel.getFavorite().observe(this, Observer { result ->
            if (result == null) {
                return@Observer
            }

            if (result.data != null) {
                adapter.setNewData(result.data)
            }
        })
    }
}