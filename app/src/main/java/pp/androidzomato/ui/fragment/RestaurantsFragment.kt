package pp.androidzomato.ui.fragment


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pp.androidzomato.OpenForTesting
import pp.androidzomato.R
import pp.androidzomato.adapter.RestaurantRecyclerAdapter
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.databinding.FragmentRestaurantsBinding
import pp.androidzomato.di.inject.Injectable
import pp.androidzomato.helper.IClickEvent
import pp.androidzomato.ui.viewmodel.RestaurantViewModel
import javax.inject.Inject

@OpenForTesting
class RestaurantsFragment : Fragment(), IClickEvent, Injectable {

    lateinit var adapter: RestaurantRecyclerAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: RestaurantViewModel
    lateinit var binding: FragmentRestaurantsBinding

    companion object {
        fun newInstance() = RestaurantsFragment()
        val TAG: String = RestaurantsFragment.toString()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RestaurantViewModel::class.java)

        adapter = RestaurantRecyclerAdapter(this)
        binding.recyclerView.adapter = adapter

        setUpViewObserver()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_restaurants, container, false)
        return binding.root
    }


    open fun setUpViewObserver() {
        viewModel.getRestaurants().observe(this, Observer { result ->
            if (result == null) {
                return@Observer
            }

            if (result.data != null) {
                adapter.setNewData(result.data)
            }
        })
    }

    override fun click(restaurantX: RestaurantX) {
        viewModel.updateRestaurantFavoriteStatus(restaurantX)
    }
}
