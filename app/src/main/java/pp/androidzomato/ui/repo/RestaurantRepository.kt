package pp.androidzomato.ui.repo

import androidx.lifecycle.LiveData
import pp.androidzomato.OpenForTesting
import pp.androidzomato.api.ZomatoApi
import pp.androidzomato.data.Restaurant
import pp.androidzomato.data.RestaurantX
import pp.androidzomato.data.SearchRestaurants
import pp.androidzomato.db.RestaurantDao
import pp.androidzomato.helper.AppExecutors
import pp.androidzomato.helper.NetworkBoundResource
import pp.androidzomato.helper.Resource
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class RestaurantRepository @Inject constructor(val api: ZomatoApi, val db: RestaurantDao, val appExecutors: AppExecutors) {
    fun getRestaurants(): LiveData<Resource<List<RestaurantX>>> {
        return object : NetworkBoundResource<List<RestaurantX>, SearchRestaurants>(appExecutors) {

            override fun saveNetworkCallResult(data: SearchRestaurants?) {
                val temp = mapToRestaurant(data!!)
                db.insertRestaurants(temp)
            }

            override fun shouldLoadFromNetwork(data: List<RestaurantX>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDatabase(): LiveData<List<RestaurantX>> {
                return db.getRestaurants()
            }

            override fun createNetworkCall(): Call<SearchRestaurants> {
                return api.getRestaurants()
            }
        }.asLiveData()
    }

    fun getFavorite(): LiveData<Resource<List<RestaurantX>>> {
        return object : NetworkBoundResource<List<RestaurantX>, List<RestaurantX>>(appExecutors) {
            override fun createNetworkCall(): Call<List<RestaurantX>> {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun saveNetworkCallResult(data: List<RestaurantX>?) {
            }

            override fun shouldLoadFromNetwork(data: List<RestaurantX>?): Boolean {
                return false
            }

            override fun loadFromDatabase(): LiveData<List<RestaurantX>> {
                return db.getFavoriteRestaurants()
            }

        }.asLiveData()
    }

    fun setFavoriteStatus(restaurantX: RestaurantX) {
        appExecutors.diskIO().execute {
            restaurantX.isFavorite = !restaurantX.isFavorite
            db.updateRestaurant(restaurantX.isFavorite, restaurantX.id)
        }
    }

    private fun mapToRestaurant(searchRestaurants: SearchRestaurants): List<RestaurantX> {
        val temp: ArrayList<RestaurantX> = ArrayList()

        for (restaurant: Restaurant in searchRestaurants.restaurants) {
            temp.add(restaurant.restaurant)
        }
        return temp
    }
}
