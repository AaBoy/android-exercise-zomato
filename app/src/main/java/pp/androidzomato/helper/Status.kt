package pp.androidzomato.helper

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}