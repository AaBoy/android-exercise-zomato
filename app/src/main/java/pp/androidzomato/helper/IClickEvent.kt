package pp.androidzomato.helper

import pp.androidzomato.data.RestaurantX

interface IClickEvent{

    fun click(restaurantX: RestaurantX)
}