package pp.androidzomato.helper

data class Error(val code: Int, val message: String)