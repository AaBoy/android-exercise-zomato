package pp.androidzomato.helper

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import android.widget.TextView
import pp.androidzomato.R

@BindingAdapter("loadImage")
fun loadImage(imageView: ImageView, imgUrl: String?) {
    GlideApp.with(imageView.context)
            .load(imgUrl)
            .into(imageView)
}

@BindingAdapter("setFavoriteText")
fun setText(textView: TextView, isFavorite: Boolean) {
    if (isFavorite) {
        textView.text = textView.context.getString(R.string.unfavored)
    } else {
        textView.text = textView.context.getString(R.string.favored)
    }

}